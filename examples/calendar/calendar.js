function {

	var date = new Date().getFullYear(),
		sel = document.getElementById('select'),
		prop = 'textContent' in document.createElement() ? 'textContent' : 'innerText',
		opt;

	for (var i = 0, len = 16; i < len; i++) {
		opt = document.createElement('option');
		opt.value = date + i;
		opt[prop] = date + i;
		sel.appendChild(opt);
	}
}