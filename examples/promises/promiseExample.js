let promise = new Promise((resolve, reject) => {
	let addition = 1 + 1;

	if (addition == 2) {
		resolve('Success');  // Resolved
	} else {
		reject('Failed');  // Rejected
	}
});

promise.then((message) => {
	console.log('then block: ' + message);
}).catch((message) => {
	console.log('catch block: ' + message);
})