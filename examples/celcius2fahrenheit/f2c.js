/* function temperatureConverter - receives the scale and the input number and converts the value and places in the DOM */
function temperatureConverter(scale, inputNumber) {

	var inputNumber = parseFloat(inputNumber);
	var convertedValue = 0;

	if (scale === 'C') {
		convertedValue = (inputNumber - 32) / 1.8;

		convertedValue = nan2zero(convertedValue);  // NaN - then set to zero

		document.getElementById("outputCelcius").innerHTML = convertedValue;
	} else if (scale = 'F') {
		convertedValue = (inputNumber * (9 / 5)) + 32

		convertedValue = nan2zero(convertedValue);  // NaN - then set to zero

		document.getElementById("outputFahrenheit").innerHTML = convertedValue;
	}
}

// NaN - then set to zero
function nan2zero(inputNumber) {
	inputNumber = +inputNumber || 0;  // NaN - then set to zero
	return inputNumber;
}