"use strict";

var num = 0;

function calc(string) {

	var obj = JSON.parse(string);

	while (obj.expr) {
		var s = JSON.stringify(obj.expr);
		calc(s);

		var n = string.lastIndexOf("expr");
		string = string.substring(0, n) + "number\": " + num + "}";
		return num;
	}

	if (obj.op === "add") {
		if (obj.number != null)
			num = obj.number + num;
		return num;

	} else {
		num = num - obj.number;
		return num;
	}
}

console.log(calc('{"op": "add", "number": 5}')); // should return 5
console.log(calc('{"op": "subtract", "number": 2}')); // should return 3
console.log(calc('{"op": "add", "number": 19}')); // should return 22
console.log(calc('{"op": "subtract", "expr" : {"op" : "add", "number" : 15}}'));

/////////////////////
///More TEST cases///

//console.log(calc('{"op":"add", "number": 0}'));  // should return 0

//console.log(calc('{"op":"add", "number": -1}'));  // should return -1

//console.log(calc('{"op":"subtract", "number": -1}'));  // should return 0

//console.log(calc('{"op":"add", "number": 5}'));  // should return 5

//console.log(calc('{"op":"subtract", "number": 10}'));  // should return -5

//console.log(calc('{"op":"add", "number": 15}'));  // should return 10
