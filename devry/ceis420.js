var x;

function sub1() {
	document.write("sub1.x = " + x + "<br />");
}

function sub2() {
	var x;
	x = 10;
	sub1();

	document.write("sub2.x = " + x + "<br />");
}

x = 5;
sub2();

document.write("global.x = " + x + "<br />");